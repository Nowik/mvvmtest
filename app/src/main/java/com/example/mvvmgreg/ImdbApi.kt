package com.example.mvvmgreg

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

public interface ImdbApi{
    companion object{
        const val BASE_URL = "http://www.omdbapi.com/"
        const val API_KEY = "63f92dc1"
    }

    @GET("?")
    fun searchMovies( @Query("apikey") apiKey : String, @Query("t") title : String ) : Call<Movie>


}