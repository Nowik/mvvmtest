package com.example.mvvmgreg

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvmgreg.databinding.MovieCellBinding

class MoviesAdapter :
    RecyclerView.Adapter<MoviesAdapter.MoviewViewHolder>() {
    var moviesList = mutableListOf<Movie>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviewViewHolder {
        Log.d("GREGMVVM", "on create view holder")
        val movieCellBinding: MovieCellBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.movie_cell,
            parent,
            false
        )

        return MoviewViewHolder(movieCellBinding)
    }

    override fun getItemCount(): Int {
        return moviesList.size
    }

    override fun onBindViewHolder(holder: MoviewViewHolder, position: Int) {
        holder.movieCellBinding.movie = moviesList[position]
    }


    class MoviewViewHolder(binding: MovieCellBinding) : RecyclerView.ViewHolder(binding.root) {
        var movieCellBinding: MovieCellBinding = binding
    }


}