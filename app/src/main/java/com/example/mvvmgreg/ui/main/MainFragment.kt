package com.example.mvvmgreg.ui.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvmgreg.Movie
import com.example.mvvmgreg.MoviesAdapter
import com.example.mvvmgreg.R
import com.example.mvvmgreg.RetrofitInstance
import kotlinx.android.synthetic.main.main_fragment.view.*


class MainFragment : Fragment(), LifecycleOwner {

    private lateinit var viewModel: MovieViewModel
    private lateinit var adapter: MoviesAdapter
    private lateinit var rootView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RetrofitInstance.startApi()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        rootView = inflater.inflate(R.layout.main_fragment, container, false)


        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = MoviesAdapter()

        rootView.moviesRecyclerView.apply {
            layoutManager = LinearLayoutManager(this@MainFragment.context, RecyclerView.VERTICAL, false)
            adapter = this@MainFragment.adapter
        }

        rootView.searchButton.setOnClickListener {
            viewModel.search(rootView.searchEditText.text.toString())
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        viewModel = ViewModelProviders.of(this)
                .get(MovieViewModel::class.java)

        viewModel.moviesLiveData.observe(this, movieListObserver) // life cycle owner


    }

    private var movieListObserver: Observer<ArrayList<Movie>> = Observer { newMoviesList ->
        Log.d("GREGMVVM", "new data ${newMoviesList.size}")
        adapter.apply {
            moviesList = newMoviesList
            notifyDataSetChanged()
        }


    }




}
