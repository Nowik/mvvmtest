package com.example.mvvmgreg.ui.main

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mvvmgreg.ImdbApi
import com.example.mvvmgreg.Movie
import com.example.mvvmgreg.RetrofitInstance
import retrofit2.Call
import retrofit2.Response

class MovieViewModel : ViewModel() {
     val moviesLiveData = MutableLiveData<ArrayList<Movie>>()

    fun search(term : String){
        Log.d("GREGMVVM", "search term $term")
        val call = RetrofitInstance.imdbApi.searchMovies(ImdbApi.API_KEY, term)
        call.enqueue(object : retrofit2.Callback<Movie>{
            override fun onFailure(call: Call<Movie>, t: Throwable) {
                Log.d("GREGMVVM","${t.message} $call")
            }

            override fun onResponse(call: Call<Movie>, response: Response<Movie>) {
                Log.d("GREGMVVM", "on response $response ${response.body()}")
                response.body()?.let { newMovie ->
                    val newList = arrayListOf<Movie>()
                    moviesLiveData.value?.let {  newList.addAll(it) }
                    newList.add(newMovie)
                    moviesLiveData.value = newList
                }

            }

        })
    }


}
