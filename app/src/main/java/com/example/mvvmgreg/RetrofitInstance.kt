package com.example.mvvmgreg

import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.security.auth.callback.Callback

object RetrofitInstance {

    lateinit var imdbApi : ImdbApi

    fun startApi(){
        val gson : Gson = GsonBuilder().setLenient().create()

        val retrofit : Retrofit = Retrofit.Builder().baseUrl(ImdbApi.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        imdbApi = retrofit.create(ImdbApi::class.java)
    }

    fun searchMovies(){

    }
}