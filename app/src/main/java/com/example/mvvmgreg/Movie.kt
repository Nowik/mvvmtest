package com.example.mvvmgreg

import com.google.gson.annotations.SerializedName

data class Movie(
    @SerializedName("Title")
    val title : String,
    @SerializedName("Year")
    val releaseYear : Int,
    @SerializedName("Genre")
    val genre : String,
    @SerializedName("Plot")
    val plot : String
)